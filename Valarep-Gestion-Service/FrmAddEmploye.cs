﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Valarep_Gestion_Service.Classes;

namespace Valarep_Gestion_Service
{
    public partial class FrmAddEmploye : Form
    {
        public Employe Employe { get; set; }
        private List<Service> services;
        private FrmEditServices frmEditServices = null;

        public FrmAddEmploye()
        {
            InitializeComponent();
        }

        private void FrmAddEmploye_Load(object sender, EventArgs e)
        {
            // simulation de l'accès à la bdd
            services = new List<Service>()
            {
                new Service() { Nom = "Administratif"},
                new Service() { Nom = "Comptabilité"},
                new Service() { Nom = "Informatique"},
            };

            cmbServices.DataSource = services;
        }

        private void btnEditServices_Click(object sender, EventArgs e)
        {
            if (frmEditServices == null || frmEditServices.IsDisposed)
            {
                frmEditServices = new FrmEditServices
                {
                    Services = services
                };
                frmEditServices.Show(); // Form_Load
                cmbServices.DataSource = frmEditServices.BindingSource;
            }
            else
            {
                frmEditServices.Focus();
            }
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            Employe = new Employe()
            {
                Nom = txtNom.Text,
                Prenom = txtPrenom.Text,
                Sexe = radHomme.Checked ? Sexe.Homme : Sexe.Femme,
                Service = (Service)cmbServices.SelectedItem
            };
        }
    }
}
