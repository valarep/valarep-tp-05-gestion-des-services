﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Valarep_Gestion_Service.Classes;

namespace Valarep_Gestion_Service
{
    public partial class FrmEditServices : Form
    {
        public List<Service> Services { get; set; }
        public BindingSource BindingSource = new BindingSource();

        private Service selectedService;

        public FrmEditServices()
        {
            InitializeComponent();
        }

        private void FrmEditServices_Load(object sender, EventArgs e)
        {
            BindingSource.DataSource = Services;
            lstServices.DataSource = BindingSource;

            /*
            foreach(Service service in Services)
            {
                service.PropertyChanged += Service_PropertyChanged;
            }
            */
        }

        private void Service_PropertyChanged(object sender, ServiceEventArgs e)
        {
        }

        private void lstServices_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstServices.SelectedIndex != -1)
            {
                selectedService = lstServices.SelectedItem as Service;
                txtNom.Text = selectedService.Nom;
            }
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            selectedService.Nom = txtNom.Text;
            BindingSource.ResetItem(lstServices.SelectedIndex);
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            BindingSource.Add(new Service()
            {
                Nom = txtNom.Text
            });
            // ResetBindings implicite
        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            /*
            DialogResult result = MessageBox.Show("Êtes-vous sûr de bien vouloir supprimer ce service ?",
                "Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                MessageBoxDefaultButton.Button2);
            */

            FrmDeleteService frm = new FrmDeleteService();
            DialogResult result = frm.ShowDialog();

            if (result == DialogResult.Yes)
            {
                BindingSource.Remove(lstServices.SelectedItem);
                // ResetBindings implicite
            }
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
