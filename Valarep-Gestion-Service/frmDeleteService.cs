﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Valarep_Gestion_Service
{
    public partial class FrmDeleteService : Form
    {
        public FrmDeleteService()
        {
            InitializeComponent();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
        }

        private void btnYes_MouseMove(object sender, MouseEventArgs e)
        {
            int x = e.Location.X;
            int y = e.Location.Y;

            Point location = btnYes.Location;

            if (x < btnYes.Width / 2) { location.X++; } else { location.X--; }
            if (y < btnYes.Height / 2) { location.Y++; } else { location.Y--; }

            if (location.X >= Width - btnYes.Width) { location.X = 12; }
            if (location.X <= 0 - btnYes.Width / 2) { location.X = Width - btnYes.Width - 24; }
            if (location.Y >= Height - 39 - btnYes.Height / 2) { location.Y = 12; }
            if (location.Y <= 0 - btnYes.Height / 2) { location.Y = Height - btnYes.Height - 39 - 12; }

            Console.WriteLine("X:" + location.X);
            Console.WriteLine("Y:" + location.Y);
            btnYes.Location = location;
        }
    }
}
