﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valarep_Gestion_Service.Classes
{
    public class Employe
    {
        // Propriétés

        public string Nom { get; set; }
        public string Prenom { get; set; }
        public Sexe Sexe { get; set; }

        // Liaison

        public Service Service { get; set; }
    }
}
