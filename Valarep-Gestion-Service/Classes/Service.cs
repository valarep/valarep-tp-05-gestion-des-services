﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valarep_Gestion_Service.Classes
{
    public class ServiceEventArgs : EventArgs
    {
        // Propriétés

        public string Property { get; private set; }
        public object OldValue { get; private set; }
        public object NewValue { get; private set; }

        // Constructeurs

        public ServiceEventArgs () : base() { }

        public ServiceEventArgs(string property, object oldValue, object newValue) : base()
        {
            Property = property;
            OldValue = oldValue;
            NewValue = newValue;
        }

        // Propriété de Classe

        public new static ServiceEventArgs Empty { get { return new ServiceEventArgs(); } }
    }

    public delegate void ServiceEventHandler(object sender, ServiceEventArgs e);

    public class Service
    {
        // Evènements

        public event ServiceEventHandler PropertyChanged; 

        // Déclenchement d'évènements
        private void RaisePropertyChanged(string property, object oldValue, object newValue)
        {
            ServiceEventArgs args = new ServiceEventArgs(property, oldValue, newValue);
            PropertyChanged?.Invoke(this, args);
        }

        // Champs

        private string nom;

        // Propriété

        public string Nom
        {
            get { return nom; }
            set
            {
                string old = nom;
                nom = value;
                RaisePropertyChanged("Nom", old, value);
            }
        }

        // Redéfinitions

        public override string ToString()
        {
            return Nom;
        }
    }
}
